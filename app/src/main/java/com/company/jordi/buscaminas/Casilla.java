/*
Copyright © 2017 Jordi García <jordigarciadev@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details.
 */




package com.company.jordi.buscaminas;

//Clase que recoje la informacion de cada celda de la matriz tablero
//sincronizados con los botones GUI mediante su tag.
public class Casilla {

    private int x,y; //coordenadas en el tablero
    private boolean esDestapado; //si el usuario lo ha destapado o no
    private boolean esBomba; //si la casilla tiene o no bomba
    private boolean hayBandera; //variable auxiliar para saber si se ha colocado una bandera en la casilla

    public Casilla(int x, int y, boolean esDestapado){ //constructor simple. El que usaremos al crear el tablero
        this.x = x;
        this.y = y;
        this.esDestapado = esDestapado;
        this.hayBandera = false;
        //esBomba = true;
    }


    public Casilla(int x, int y, boolean esDestapado, boolean esBomba){ //contructor auxiliar. Si fuera necesario inicializar esBomba a true.
        this.x = x;
        this.y = y;
        this.esDestapado = esDestapado;
        this.esBomba  = esBomba;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean getEsDestapado() {
        return esDestapado;
    }

    public void setEsDestapado(boolean esDestapado) {
        this.esDestapado = esDestapado;
    }

    public boolean getEsBomba() {
        return esBomba;
    }

    public void setEsBomba(boolean esBomba) {
        this.esBomba = esBomba;
    }

    public boolean getHayBandera() {
        return hayBandera;
    }

    public void setHayBandera(boolean hayBandera) {
        this.hayBandera = hayBandera;
    }
}
