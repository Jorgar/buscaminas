/*
Copyright © 2017 Jordi García <jordigarciadev@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details.
 */



package com.company.jordi.buscaminas;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;


/*
    DialogFragment personalizado
    con las dificultades del juego
 */

public class Dificultad extends DialogFragment {

    RespuestaDialogo respuesta; //interface que comunica la activity con el DialogFragment
    private int dificultad;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final String[] colores = getResources().getStringArray(R.array.nivelDificultad);
        AlertDialog.Builder constructor = new AlertDialog.Builder(getActivity(), R.style.DialogoPersonalizadoOpciones);
        constructor.setSingleChoiceItems(colores, dificultad,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int opc) {
                        respuesta.onRespuesta(opc); //pasamos la eleccion a la interface que la enviará a su vez a la activity.
                        dialog.dismiss();
                    }
                });
        return constructor.create();
    }

    public interface RespuestaDialogo{
        public void onRespuesta(int opcion);
    }

    public void onAttach(Context entorno){
        super.onAttach(entorno);
        respuesta = (RespuestaDialogo)entorno;
    }

    public void setDificultad(int dif){
        dificultad = dif;
    } //para que quede marcada la ultima opcion guardada en el fragment

}
