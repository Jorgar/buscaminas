/*
Copyright © 2017 Jordi García <jordigarciadev@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details.
 */


package com.company.jordi.buscaminas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MenuPrincipal extends AppCompatActivity implements Dificultad.RespuestaDialogo{

    int nivelDificultad = 0; //0 fácil, 1 normal, 2 dificil. Por defecto 0

    //Facil: 10 bombas, 600 segundos (10 minutos)
    //Medio: 15 bombas, 300 segundos (5 minutos)
    //Dificil: 30 bombas, 150 segundos (2 minutos y 30 segundos)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mp_lay);
    }

    public void clickJugar(View v){
        Intent aPartida = new Intent(this, Partida.class);
        aPartida.putExtra("dificultad", nivelDificultad);
        startActivity(aPartida);
    }

    public void clickOpciones(View v){
        Dificultad menuElegir = new Dificultad();   //Llamamos a la clase que contiene el DialogFragment
        menuElegir.setDificultad(nivelDificultad);  //marcamos la ultima opcion guardada de dificultad si la hubiese
        menuElegir.show(getFragmentManager(), "Mi Dialogo");    //Llamamos al fragment por pantalla
    }

    @Override
    public void onRespuesta(int opcion){
        nivelDificultad = opcion;
    }

}
