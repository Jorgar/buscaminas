/*
Copyright © 2017 Jordi García <jordigarciadev@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.

This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details.
 */




package com.company.jordi.buscaminas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Partida extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener, Dificultad.RespuestaDialogo{

    private Casilla[][] tablero = new Casilla[8][8];
    GridLayout tableroGrafico;
    private int casillasParaGanar;
    private int casillasDestapadas;
    private int numeroBombas = 10;
    private boolean ganar, perder;
    private int tiempo; //segundos
    private int dificultad;
    TextView cuentaAtras;
    CountDownTimer cronometro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.part_lay);
        ganar = false;
        perder = false;
        cuentaAtras = (TextView)findViewById(R.id.contador);
        Intent intent = getIntent();
        dificultad = intent.getIntExtra("dificultad",-1);

        switch (dificultad){
            case 0:
                numeroBombas = 10;
                tiempo = 600*1000+1000; //+1000 para dar un pequeño delay
                break;
            case 1:
                numeroBombas = 15;
                tiempo = 300*1000+1000;
                break;
            case 2:
                numeroBombas = 30;
                tiempo = 150*1000+1000;
                break;
        }


        tableroGrafico = (GridLayout)findViewById(R.id.tablero);
        casillasParaGanar = 64-numeroBombas;
        casillasDestapadas = 0;
        rellenarTablero(numeroBombas);
        crearTableroGrafico();

        cronometro = new CountDownTimer(tiempo, 1000) {

            public void onTick(long millisUntilFinished) {
                cuentaAtras.setText(millisUntilFinished / 1000+"");
            }

            public void onFinish() {
                cuentaAtras.setText(0+"");
                perder = true;
                Toast toastPerder = Toast.makeText(getApplicationContext(),"¡Se acabó el tiempo!", Toast.LENGTH_SHORT);
                toastPerder.show();
            }
        }.start();
    }

    private void rellenarTablero(int numeroBombas){
        int xAle,yAle;
        int contadorBombas = numeroBombas;

        for (int x = 0; x < tablero.length; x++){ //rellenamos toda la matriz con Casilla.
            for (int y=0; y<tablero[x].length; y++){
                tablero[x][y] = new Casilla(x,y,false,false); //Casillas sin bombas inicialmente.
            }
        }

        xAle = (int) (Math.random() * 7); //random del 0 al 7
        yAle = (int) (Math.random() * 7);

        //Insertamos las bombas
        while(contadorBombas>0) {
            if(!tablero[xAle][yAle].getEsBomba()){
                tablero[xAle][yAle].setEsBomba(true);
                contadorBombas--;
            }
            xAle = (int) (Math.random() * 7);
            yAle = (int) (Math.random() * 7);
            System.out.println("bomba x:" + xAle +"    y:" + yAle);                         //borrar
        }
    }

    private void destaparCasilla(int x, int y){
        //si no es bomba
        int bombasAlrededor = 0;
        Button boton = (Button)findViewById(Integer.parseInt(x+""+y));
        if(!(tablero[x][y].getEsBomba()) && !(tablero[x][y].getEsDestapado())){ //no es bomba
            tablero[x][y].setEsDestapado(true);
            casillasDestapadas++;
            boton.setBackground(getResources().getDrawable(R.drawable.boton_destapado_puntuacion));
            //contamos bombas alrededor
            for(int j = y-1; j<=y+1;j++){ //j es igual a coordeanda Y
                for(int i=x-1;i<=x+1;i++){//i es igual a coordenada X
                    if(!(j>tablero.length-1 || j<0 || i>tablero.length-1 || i<0)){
                        System.out.println("x: " + i + "   y: " + j);                       //borrar
                        if(tablero[i][j].getEsBomba()){
                            bombasAlrededor++;
                        }
                    }
                }
            }
            System.out.println("Bombas alrededor: " + bombasAlrededor);                     //borrar

            if(bombasAlrededor == 0){
                //abrir
                boton.setBackground(getResources().getDrawable(R.drawable.boton_destapado));
                for(int j = y-1; j<=y+1;j++) { //j es igual a coordeanda Y
                    for (int i = x - 1; i <= x + 1; i++) {//i es igual a coordenada X
                        if (!(j > tablero.length - 1 || j < 0 || i > tablero.length - 1 || i < 0)) {
                            destaparCasilla(i,j);
                        }
                    }
                }
            }else{
                boton.setText(bombasAlrededor+"");
                boton.setTextColor(Color.BLACK);
                boton.setGravity(Gravity.CENTER);
            }

        }else if(tablero[x][y].getEsBomba() && !(tablero[x][y].getEsDestapado())){  //es bomba
            boton.setBackground(getResources().getDrawable(R.drawable.boton_bomba));
            perder = true;
            cronometro.cancel();
            Toast toastPerder = Toast.makeText(getApplicationContext(),"Has perdido", Toast.LENGTH_SHORT);
            toastPerder.show();
        }

        if(casillasDestapadas == casillasParaGanar){
            ganar = true;
            cronometro.cancel();
            Toast toastGanar = Toast.makeText(getApplicationContext(),"¡Has ganado!", Toast.LENGTH_SHORT);
            toastGanar.show();
        }
    }

    private void crearTableroGrafico(){
        for (int x = 0; x < tablero.length; x++){ //rellenamos toda la matriz con Casilla.
            for (int y=0; y<tablero[x].length; y++){
                Button botonCasilla = new Button(this);
                botonCasilla.setBackground(getResources().getDrawable(R.drawable.boton_normal));
                botonCasilla.setText("");
                botonCasilla.setTag(x+","+y);
                botonCasilla.setId(Integer.parseInt(x+""+y));
                botonCasilla.setOnClickListener(this);
                botonCasilla.setOnLongClickListener(this);
                //botonCasilla.setTextAlignment(View.TEXT_ALIGNMENT_INHERIT);

                GridLayout.LayoutParams params = new GridLayout.LayoutParams();
                params.setMargins(10,10,10,10);

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int ancho = size.x;
                int alto = size.y;

                if(ancho>=720 && alto<=1280){
                    params.height = 60;
                    params.width = 60;
                    botonCasilla.setTextSize(8);
                }else {
                    params.height = 90;
                    params.width = 90;
                }

                params.setGravity(Gravity.CENTER);
                params.columnSpec = GridLayout.spec(y);
                params.rowSpec = GridLayout.spec(x);

                botonCasilla.setLayoutParams(params);
                tableroGrafico.addView(botonCasilla);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(!ganar && !perder) {
            int x, y;
            x = Integer.parseInt(view.getTag().toString().substring(0, 1));
            y = Integer.parseInt(view.getTag().toString().substring(view.getTag().toString().length() - 1, view.getTag().toString().length()));
            System.out.println(x + "," + y);                                                       //borrar
            destaparCasilla(x, y);
        }
    }

    @Override
    public boolean onLongClick(View view) {
        int x,y;
        x = Integer.parseInt(view.getTag().toString().substring(0,1));
        y = Integer.parseInt(view.getTag().toString().substring(view.getTag().toString().length()-1,view.getTag().toString().length()));
        if(!ganar && !perder) {
            if (!tablero[x][y].getEsDestapado() && !tablero[x][y].getHayBandera()) {
                Button boton = (Button) findViewById(Integer.parseInt(x + "" + y));
                boton.setBackground(getResources().getDrawable(R.drawable.boton_bandera));
                tablero[x][y].setHayBandera(true);
            } else if (!tablero[x][y].getEsDestapado() && tablero[x][y].getHayBandera()) {
                Button boton = (Button) findViewById(Integer.parseInt(x + "" + y));
                boton.setBackground(getResources().getDrawable(R.drawable.boton_normal));
                tablero[x][y].setHayBandera(false);
            }
        }
        return true;
    }

    public void nuevoJuego(View v){
        Intent intent=getIntent();
        finish();
        startActivity(intent);
    }

    public void b_ajustes(View v){
        Dificultad menuElegir = new Dificultad();
        menuElegir.setDificultad(dificultad);
        menuElegir.show(getFragmentManager(), "Mi Dialogo");
    }

    @Override
    public void onRespuesta(int opcion){
        dificultad = opcion;
        Intent intento = new Intent(this, Partida.class);
        intento.putExtra("dificultad", dificultad);
        finish();
        startActivity(intento);
    }

    public void clickInfo(View v){
        // custom dialog
        final Context context = this;
        final Dialog dialog = new Dialog(context) {
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                // Tap anywhere to close dialog.
                this.dismiss();
                return true;
            }
        };
        dialog.setContentView(R.layout.tutorial);
        dialog.show();
    }
}
