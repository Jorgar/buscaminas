# **BUSCAMINAS (ANDROID)** #

_________________________________________________________________

*El juego del buscaminas de toda la vida para SO Android.*

*Cualquiera puede usarlo para lo que quiera y como quiera.*

*Desarrollado en Android Studio 2.2*


*Publicado bajo la licencia [WTFPL!](http://www.wtfpl.net/)*

_________________________________________________________________

![Screenshot_20170313-213120.png](https://bitbucket.org/repo/RbEGEG/images/1283688224-Screenshot_20170313-213120.png)         ![Screenshot_20170313-213127.png](https://bitbucket.org/repo/RbEGEG/images/1094446394-Screenshot_20170313-213127.png)

_________________________________________________________________

*Repo owner: Jorgar (Jordi García)*

*Contact: jordigarciadev@gmail.com*